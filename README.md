# Pedanticbot

## What is Pedanticbot?

Pedanticbot is a bot that injests communiqués  then returns useful info regarding syntax, grammer, punctuation, spelling, and even style, depending on configuration.

Pedanticbot can be installed and run on any server, using either nginx or apache (see [Configuration](#configuration) below).

## Installation

### Ubuntu, Debian

```
apt install pedanticbot-server pedanticbot-client
```

### Arch, Artix, Void, etc.

```
pacman -Syu pedanticbot-server pedanticbot-client
```

### Manual Installation

```
git clone https://gitlab.com/tuckerw/pedanticbot
cd pedanticbot
configure
make
sudo make install
```

## Configuration

Configuration options can be set in `pedanticbot.conf`.
Available options are commented out in the file.

Currently available options:

```
server # available options: nginx, apache, none
syntax # available options: on, off
grammer # available options: on, off
punctuation # available options: on, off
style # available options: on, off
autoreply # on, off | whether to automatically send feedback to source of communiqué
```

## Usage

Pedanticbot can be configured to work with IRC, Discord, Slack, emails, and SMS.
Implementation examples can be found in the `examples/` directory.

